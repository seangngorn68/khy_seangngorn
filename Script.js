var clickbtn = document.getElementById("btncal");
var btnsValue = document.getElementById("btn-value");
var icon = document.getElementById("btn-icon");
var startAtH = document.getElementById("start-time-h");
var startAtM = document.getElementById("start-time-m");
var start = document.getElementById("start-time-s");
var stopAtM = document.getElementById("stop-time-m");
var stopT = document.getElementById("stop-time-s");
var stopAtH = document.getElementById("stop-time-h");
var minutes = document.getElementById("minute-time");
var S = document.getElementById("minute-s");
var reals = document.getElementById("reals-time");

var mycount;
var timingdate;
var startTime;
var endTime;
var calTime;

// livedate
function myClearscreen() {
  startAtH.innerHTML = "00";
  startAtM.innerHTML = "00";
  start.innerHTML = "00";
  stopAtH.innerHTML = "00";
  stopAtM.innerHTML = "00";
  stopT.innerHTML = "00";
  reals.innerHTML = "0";
  minutes.innerHTML = "0";
}

setInterval(time, 1000);

function time() {
  var date = new Date();
  document.getElementById("dayname").innerHTML =
    date.toDateString() + " " + date.toLocaleTimeString();
}

// control button
function btnclick() {
  if (btnsValue.textContent == "Start") {
    var pickDate = new Date();
    var hours = pickDate.getHours() % 12 || 12;

    icon.classList.add("fa-circle-stop");
    icon.classList.remove("fa-play");
    btnsValue.innerHTML = "Stop";
    clickbtn.style.backgroundColor = "red";

    startAtH.innerHTML = hours;
    startAtM.innerHTML = pickDate.getMinutes();
    start.innerHTML = pickDate.getSeconds();
    startTime = pickDate;

  } else if (btnsValue.textContent == "Stop") {
    timingdate = new Date();
    stopAtH.innerHTML = timingdate.getHours() % 12 || 12;
    stopAtM.innerHTML = timingdate.getMinutes();
    stopT.innerHTML = timingdate.getSeconds();
    icon.classList.add("fa-trash");
    icon.classList.remove("fa-circle-stop");
    btnsValue.innerHTML = "Clear";
    clickbtn.style.backgroundColor = "black";
    setTimeout(() => {
      clearInterval(mycount);
    }, 1);

    endTime = new Date();
    let resultDiffDate = endTime.getTime() - startTime.getTime(); //Result will be milliseconds
    let totalMinute = Math.round(resultDiffDate / 60000); //1000 milliseconds = 1 second, 60 seconds = 1 minute
  
    totalMinute = 21;
    if (totalMinute <= 15) {
      reals.innerHTML = 500;
    } else if (totalMinute <= 30) {
      reals.innerHTML = 1000;
    } else if (totalMinute <= 60) {
      reals.innerHTML = 1500;
    } else {
      let overPriceInt = Math.floor(totalMinute / 60);
      let overPriceFloat = Math.floor(totalMinute % 60);
        if(overPriceFloat > 0){
            if (overPriceFloat <= 15) {
                reals.innerHTML = 500 +( 1500 * overPriceInt);
              } else if (overPriceFloat <= 30) {
                reals.innerHTML = 1000+(1500* overPriceInt);
              } else if (overPriceFloat <= 60) {
                reals.innerHTML = 1500+(1500* overPriceInt);
              }
        }
        else{
            reals.innerHTML=1500*overPriceInt;    
        }
    }
    minutes.innerHTML = totalMinute;
  } else if (btnsValue.textContent == "Clear") {
    icon.classList.add("fa-play");
    icon.classList.remove("fa-trash");
    btnsValue.innerHTML = "Start";
    clickbtn.style.backgroundColor = "green";
    myClearscreen();
  }
}
